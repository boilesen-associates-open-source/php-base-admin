# PHP Base Admin

Simple project with database connection, user creation and authentication and administrative backend.

## How to Install and run

### 1. Create a host entry for your project

We will use the path `http://dev.pba`. So, our entry will be:

`127.0.0.1	dev.pba`

### 2. Create a database for your project

Create a user and a database for your project:

#### PostgreSQL

`create user pba with password 'pba_password' createdb;`

`create database pba_development with owner pba;`

#### MySQL

`CREATE USER 'pba'@'localhost' IDENTIFIED BY 'pba_password';`

`CREATE DATABASE pba_development`;

`GRANT ALL PRIVILEGES ON *.pba_development TO 'pba'@'localhost' IDENTIFIED BY 'pba_password';`

### 3. Configure your project

In your source folder, copy `example.pba.config.php` to `pba.config.php` and make the necessary changes.

### 4. Nginx and PHP 7.x

Create the following nginx conf file `dev.pba.conf`:

```
server {

	listen 80;

	server_name dev.pba;

	root /usr/share/nginx/html/src/php-base-admin;


	location / {
		index index.php index.html;

		try_files /public/$uri /public/$uri/ /public/index.php?$request_uri;
	}


	location /admin/assets/ {
		rewrite ^/admin(/.*)$ $1 break;
		root /usr/share/nginx/html/src/php-base-admin/admin/;
		try_files $uri $uri/ =404;
	}


	location ~ .php$ {

		fastcgi_connect_timeout 360s;		 # default of 60s is just too long

		fastcgi_read_timeout 360s;			 # default of 60s is just too long

		fastcgi_pass unix:/run/php/php7.2-fpm.sock;

		fastcgi_index	index.php;

		fastcgi_param	SCRIPT_FILENAME	$document_root$fastcgi_script_name;

		include fastcgi_params;

	}

}
```

Restart nginx to enable your project.

**Hint:** Remember to make `display_errors On;` in your php.ini file.


### 5. Composer and Yarn

Go to your source folder and run `composer install`. Here composer will hint you if you have any php extension missing.

Then, go to `admin` folder and run `yarn install` and then `yarn dev run`.


### 6. Database initialization

Initialize your database with `phinx` by running:

`php vendor/robmorgan/bin/phinx migrate`

And then, populate your database:

`php vendor/robmorgan/bin/phinx seed:run`

### 7. Ready, set and go!

Now, test your login with the following credentials:

**username:** *admin*
**password:** *admin123*

Remember to change your password and your e-mail in your first login.
