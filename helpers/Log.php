<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This Log.php is a simple wrapper for Monolog logs.
	 *
	 * Monolog Logging
	 *
	 *	DEBUG (100): Detailed debug information.

		INFO (200): Interesting events. Examples: User logs in, SQL logs.

		NOTICE (250): Normal but significant events.

		WARNING (300): Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.

		ERROR (400): Runtime errors that do not require immediate action but should typically be logged and monitored.

		CRITICAL (500): Critical conditions. Example: Application component unavailable, unexpected exception.

		ALERT (550): Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.

		EMERGENCY (600): Emergency: system is unusable.
	 *
	 */

	class Log{
		private static $stream = null;
		private static $cron_stream = null;

		public static function init(){
			if (self::$stream == null){
				self::$stream = new \Monolog\Logger('PBA');
				self::$stream->pushHandler(new \Monolog\Handler\StreamHandler($_ENV["root"].'/log/app.'.$_ENV["PBA_ENV"].'.'.date('Y-m-d').'.log'));
			}
 			if (self::$cron_stream == null){
				self::$cron_stream = new \Monolog\Logger('PBA');
				self::$cron_stream->pushHandler(new \Monolog\Handler\StreamHandler($_ENV["root"].'/log/cron.app.'.$_ENV["PBA_ENV"].'.'.date('Y-m-d').'.log'));
			}
			return true;
		}

		public static function error($string){
			if (self::$stream == null) self::init();
			return self::$stream->error($string);
		}

		public static function info($string){
			if (self::$stream == null) self::init();
			return self::$stream->info($string);
		}

		public static function cronError($string){
			if (self::$cron_stream == null) self::init();
			return self::$cron_stream->error($string);
		}
		public static function cronInfo($string){
			if (self::$cron_stream == null) self::init();
			return self::$cron_stream->info($string);
		}
	}
?>
