<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This Mail.php class is a simple wrapper for PHPMailer.
	 */
	class Mail {
		private $mail;

		public function __construct($subject, $body, $alt_body){
			$this->mail = new PHPMailer();
			$this->mail->CharSet = 'UTF-8';
			/* Set SMTP connection */
			$this->mail->isSMTP();
			$this->mail->SMTPAuth = true;
			$this->mail->SMTPDebug = false;

			$this->mail->Host = $_ENV["PBA_MAIL_SMTP_HOST"];
			$this->mail->Username = $_ENV["PBA_MAIL_SMTP_USER"];
			$this->mail->Password = $_ENV["PBA_MAIL_SMTP_PASSWORD"];
			$this->mail->SMTPSecure = $_ENV["PBA_MAIL_SMTP_ENCRYPTION"];
			$this->mail->Port = $_ENV["PBA_MAIL_SMTP_PORT"];
			/* Set message headers */
			$this->mail->setFrom($_ENV["PBA_MAIL_FROM_EMAIL"], $_ENV["PBA_MAIL_FROM_NAME"]);
			$this->mail->addReplyTo($_ENV["PBA_MAIL_REPLY_TO_EMAIL"], $_ENV["PBA_MAIL_REPLY_TO_NAME"]);
			/* Set mail body */
			$this->mail->isHTML(true);
			$this->mail->Subject = $subject;
			$this->mail->Body = $body;
			$this->mail->AltBody = $alt_body;
		}

		public function addAttachment($file_path){
			$this->mail->addAttachment($file_path);
		}

		public function addAddress($name, $email){
			$this->mail->addAddress($email, $name);
		}

		public function addCC($name, $email){
			$this->mail->addCC($email, $name);
		}

		public function addBCC($name, $email){
			$this->mail->addBCC($email, $name);
		}

		public function send(){
			if ($this->mail->send()){
				return true;
			}
			Log::error("PBA E-mail sending error: ".$this->mail->ErrorInfo);
			return false;
		}
	}
?>
