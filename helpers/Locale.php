<?php
class Locale{
	public static function console_log( $data ){
			 echo '<script>';
			 echo 'console.log('. json_encode( $data ) .')';
			 echo '</script>';
		 }

		public static function setLocalization($code) {
				$langCodes = array(
														 "en" => "en_US.utf8",
														 "pt" => "pt_BR.utf8"
													 );
				if(!isset($langCodes[strtolower($code)])) {
						return;
				}
				$baseLangDir = $_ENV["root"].'/locale';
				$appName     = "php-base-admin";
				$lang = $langCodes[strtolower($code)];
				// Set language
				putenv('LANGUAGE='.$lang);
				setlocale(LC_ALL, $lang);

				#console_log($lang);
				#console_log($baseLangDir);

				// Specify the location of the translation tables
				bindtextdomain($appName, realpath($baseLangDir));
				bind_textdomain_codeset($appName, 'UTF-8');
				// Choose domain
				textdomain($appName);
		}
	}
?>
