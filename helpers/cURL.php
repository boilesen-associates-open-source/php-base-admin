<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This cURL.php is a simple cURL wrapper.
	 */
	class cURL{
		public static function post($url, $data, $auth=false, $json=false){
			if ((filter_var($url, FILTER_VALIDATE_URL) === true) && is_array($data)){
				$curl = curl_init();
				$curl_options = array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $url,
					CURLOPT_POST => sizeof($data),
					CURLOPT_POSTFIELDS => $data
				);
				if (is_array($auth) && count($auth)>0){
					$curl_options[CURLOPT_HTTPHEADER] = $auth;
				}
				curl_setopt_array($curl, $curl_options);

				if ($json == true){
					$result = json_decode(curl_exec($curl));
				}else{
					$result = curl_exec($curl);
				}

				curl_close($curl);
				return $result;
			}
			return false;
		}

		public static function get($url, $data, $json=false){
		}

		public static function put($url, $data, $json=false){
		}

		public static function patch($url, $data, $json=false){
		}

		public static function delete($url, $data, $json=false){
		}

		public static function file_get_contents_curl($url){
		    $ch = curl_init();

		    curl_setopt($ch, CURLOPT_HEADER, 0);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

		    $data = curl_exec($ch);
		    curl_close($ch);

		    return $data;
		}
	}
?>
