<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This app.boot.php is the PBA bootstrap file.
	 * Here we make all the environment setup with logs, localization, error treatment
	 * and database connection.
	 */

	use Monolog\Logger;
	use Monolog\Handler\StreamHandler;

	setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
	date_default_timezone_set('America/Sao_Paulo');
	/*setlocale(LC_MESSAGES, $_ENV["PBA_LOCALE_LANGUAGE"]);
	bindtextdomain($_ENV["PBA_LOCALE_DOMAIN"], $_ENV["PBA_LOCALE_DIRECTORY"]);
	textdomain($_ENV["PBA_LOCALE_DOMAIN"]);
	bind_textdomain_codeset($_ENV["PBA_LOCALE_DOMAIN"], 'UTF-8');*/

	/* PBA Helpers: your helper goes here */
	require $_ENV["root"].'/helpers/Locale.php';
	require $_ENV["root"].'/helpers/JSON.php';
	require $_ENV["root"].'/helpers/Crypt.php';
	require $_ENV["root"].'/helpers/Log.php';
	require $_ENV["root"].'/helpers/Database.php';
	require $_ENV["root"].'/helpers/Gitlab.php';
	require $_ENV["root"].'/helpers/IssueReport.php';
	require $_ENV["root"].'/helpers/cURL.php';
	require $_ENV["root"].'/helpers/StringUtils.php';
	require $_ENV["root"].'/helpers/Mail.php';

	/* Initialize Logger */
	Log::init();

	/* SlimFramework: Creating SlimFramework API Application */
	$app_config = array();
	$app = new \Slim\App($app_config);

	/* Getting Container */
	$container = $app->getContainer();

	/* SlimFramework: Setting up CSRF Guard */
	$container['csrf'] = function ($container) {
		return new \Slim\Csrf\Guard;
	};

	/* SlimFramework: Telling error handler to redirect to page error 500 */
	$container['errorHandler'] = function ($container) {
		return function ($request, $response, $exception) use ($container) {
			header("Location: /500");
			return $container['response']->withHeader('Content-Type', 'text/html');
		};
	};

	/* SlimFramework: Telling error handler to redirect to page error 500 */
	$container['notFoundHandler'] = function ($container) {
		return function ($request, $response) use ($container) {
			header("Location: /404");
		    return $container['response']->withHeader('Content-Type', 'text/html');;
		};
	};
	/* PHP-PDO: Testing database connection */
	try{
		/* Try to connect with database */
		$database = Database::connect();
	}catch(\PDOException $e){
		Log::error("PBA: Unable to connect to database | ". $e->getMessage());
	}

	Locale::setLocalization("pt");

?>
