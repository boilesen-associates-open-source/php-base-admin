<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This migration creates the database app schema and its first tables
	 */
use Phinx\Migration\AbstractMigration;

class DatabaseCreation extends AbstractMigration{
		public function up(){
			$this->execute("CREATE SCHEMA IF NOT EXISTS app;");
			$this->execute("ALTER SCHEMA app OWNER TO pba;");
			$this->execute("CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;");
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			/*
			 * Creating tables...
			 */
			$reference_class = $this->table('reference_class');
			$reference_class->addColumn('title', 'string', ['limit' => 255,'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('name', 'string', ['limit' => 255, 'null' => false])
			 	->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->addIndex(['name'], ['unique' => true])
			 	->save();

			$reference_class_field = $this->table('reference_class_field');
			$reference_class_field->addColumn('id_reference_class', 'integer', ['null' => false])
				->addColumn('title', 'string', ['limit' => 255,'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('name', 'string', ['limit' => 255, 'null' => false])
				->addColumn('type', 'string', ['limit' => 255, 'null' => false])
				->addIndex(['id_reference_class','name'], ['unique' => true])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$email_user_data = $this->table('email_user_data');
			$email_user_data->addColumn('id_email_user', 'integer', ['null' => false])
				->addColumn('id_reference_class', 'integer', ['null' => false])
				->addColumn('object_id', 'integer', ['null' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			/* email_template */
			$email_template = $this->table('email_template');
			$email_template->addColumn('title', 'string', ['limit' => 255, 'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('subject', 'string', ['limit' => 255, 'null' => false])
				->addColumn('markdown', 'boolean', ['default' => false])
				->addColumn('html_body', 'string', ['limit' => 100000, 'null' => false ])
				->addColumn('text_body', 'string', ['limit' => 100000, 'null' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$email_user = $this->table('email_user');
			$email_user->addColumn('id_email_template', 'integer', ['null' => false])
				->addColumn('id_user', 'integer', ['null' => false])
				->addColumn('sent', 'boolean', ['default' => false])
				->addColumn('sent_at', 'timestamp', ['timezone' => true, 'null' => true])
				->addColumn('parse_successful', 'boolean', ['default' => false])
				->addColumn('error', 'boolean', ['default' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$image = $this->table('image');
			$image->addColumn('title', 'string', ['limit' => 255, 'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('filename', 'string', ['limit' => 255, 'null' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$notification = $this->table('notification');
			$notification->addColumn('title', 'string', ['limit' => 255, 'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('type', 'integer', ['null' => false])
				->addColumn('id_email_template', 'integer', ['null' => true])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$user = $this->table('user');
			$user->addColumn('name', 'string', ['limit' => 500, 'null' => false])
				->addColumn('email', 'string', ['limit' =>255, 'null' => false])
				->addColumn('username', 'string', ['limit' =>20, 'null' => false])
				->addColumn('password', 'string', ['limit' =>200, 'null' => false])
				->addColumn('id_profile_picture', 'integer', ['null' => true])
				->addColumn('bio', 'string', ['limit' => 5000, 'null' => true])
				->addColumn('linkedin', 'string', ['limit' => 500, 'null' => true])
				->addColumn('facebook', 'string', ['limit' => 500, 'null' => true])
				->addColumn('twitter', 'string', ['limit' => 500, 'null' => true])
				->addColumn('youtube', 'string', ['limit' => 500, 'null' => true])
				->addColumn('email_confirmation', 'string', ['limit' => 200, 'null' => true])
				->addColumn('reset_password', 'string', ['limit' => 200, 'null' => true])
				->addColumn('notify_system_events', 'boolean', ['default' => false])
				->addColumn('notify_lead_events', 'boolean', ['default' => false])
				->addColumn('notify_contact_events', 'boolean', ['default' => false])
				->addColumn('id_role', 'integer', ['null' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$user_notification = $this->table('user_notification');
			$user_notification->addColumn('id_user', 'integer', ['null' => false])
				->addColumn('id_notification', 'integer', ['null' => false])
				->addColumn('id_email_user', 'integer', ['null' => true])
				->addColumn('read', 'boolean', ['default' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$user_notification_data = $this->table('user_notification_data');
			$user_notification_data->addColumn('id_user_notification', 'integer', ['null' => false])
				->addColumn('id_reference_class', 'integer', ['null' => false])
				->addColumn('object_id', 'integer', ['null' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();


			$role = $this->table('role');
			$role->addColumn('name', 'string', ['limit' => 255,'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('created_at', 'timestamp', ['timezone' => true, 'default' => 'CURRENT_TIMESTAMP', 'null' => false])
				->save();

			$scope = $this->table('scope');
			$scope->addColumn('name', 'string', ['limit' => 255,'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->save();

			$permission = $this->table('permission');
			$permission->addColumn('name', 'string', ['limit' => 255,'null' => false])
				->addColumn('description', 'string', ['limit' => 500, 'null' => false])
				->addColumn('id_scope', 'integer', ['null' => false])
				->save();
			$permission->addForeignKey(['id_scope'], 'scope', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$role_permission = $this->table('role_permission');
			$role_permission->addColumn('id_role', 'integer', ['null' => false])
				->addColumn('id_permission', 'integer', ['null' => false])
				->save();
			$role_permission->addForeignKey(['id_role'], 'role', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();
			$role_permission->addForeignKey(['id_permission'], 'permission', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			/* Indexes */

			$user->addIndex(['email_confirmation'], ['unique' => true, 'name' => 'user_email_confirmation_key'])->save();

			$user->addIndex(['email'], ['unique' => true, 'name' => 'user_email_key'])->save();

			$user->addIndex(['reset_password'], ['unique' => true, 'name' => 'user_reset_password_key'])->save();

			$user->addIndex(['username'], ['unique' => true, 'name' => 'user_username_key'])->save();

			/* Foreign Keys */
			$reference_class_field->addForeignKey(['id_reference_class'], 'reference_class', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$email_user_data->addForeignKey(['id_email_user'], 'email_user', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();
			$email_user_data->addForeignKey(['id_reference_class'], 'reference_class', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$email_user->addForeignKey(['id_email_template'], 'email_template', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$email_user->addForeignKey(['id_user'], 'user', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$notification->addForeignKey(['id_email_template'], 'email_template', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user->addForeignKey(['id_profile_picture'], 'image', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user->addForeignKey(['id_role'], 'role', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user_notification->addForeignKey(['id_notification'], 'notification', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user_notification->addForeignKey(['id_user'], 'user', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user_notification->addForeignKey(['id_email_user'], 'email_user', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user_notification_data->addForeignKey(['id_user_notification'], 'user_notification', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();

			$user_notification_data->addForeignKey(['id_reference_class'], 'reference_class', 'id', ['delete' => 'RESTRICT', 'update' => 'CASCADE'])->save();
		}
}
