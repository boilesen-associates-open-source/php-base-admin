<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This seed populates our reference_class_field table
	 */
use Phinx\Seed\AbstractSeed;

class ReferenceClassFields extends AbstractSeed{

		public function getDependencies(){
			return [
				'ReferenceClass'
			];
		}
		/**
		 * Run Method.
		 *
		 * Write your database seeder using this method.
		 *
		 * More information on writing seeders is available here:
		 * http://docs.phinx.org/en/latest/seeding.html
		 */
		public function run(){
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			$reference_class_field = $this->table('reference_class_field');
			/* ReferenceClass: User */
			$reference_class_field_row_1 = array(
				"id_reference_class" => 1,
				"title" => "Name",
				"description" => "User readable name",
				"name" => "name",
				"type" => "string"
			);
			$reference_class_field->insert($reference_class_field_row_1);
			$reference_class_field_row_2 = array(
				"id_reference_class" => 1,
				"title" => "Email Confirmation Link",
				"description" => "Link for user email confirmation",
				"name" => "email_confirmation_link",
				"type" => "string"
			);
			$reference_class_field->insert($reference_class_field_row_2);
			$reference_class_field_row_3 = array(
				"id_reference_class" => 1,
				"title" => "Reset Password Link",
				"description" => "Link for user password reset",
				"name" => "reset_password_link",
				"type" => "string"
			);
			$reference_class_field->insert($reference_class_field_row_3)->saveData();
		}
}
