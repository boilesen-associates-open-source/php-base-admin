<?php


use Phinx\Seed\AbstractSeed;

class EmailScopePermission extends AbstractSeed
{
	public function getDependencies(){
		return [
			'AdminUserScopePermission'
		];
	}
		/**
		 * Run Method.
		 *
		 * Write your database seeder using this method.
		 *
		 * More information on writing seeders is available here:
		 * http://docs.phinx.org/en/latest/seeding.html
		 */
		public function run() {
			$this->getAdapter()->setOptions(array_replace($this->getAdapter()->getOptions(), ['schema' => 'app']));

			$scope = $this->table('scope');
			$scope_row_1 = array(
				"name" => "Email Templates Management",
				"description" => "Email Templates management methods scope"
			);
			$scope->insert($scope_row_1)->saveData();

			$permission = $this->table('permission');

			$permission_row_1 = array(
				"name" => "Create",
				"description" => "Create email templates",
				"id_scope" => 3
			);
			$permission->insert($permission_row_1);

			$permission_row_2 = array(
				"name" => "Read",
				"description" => "Read emails data",
				"id_scope" => 3
			);
			$permission->insert($permission_row_2);

			$permission_row_3 = array(
				"name" => "Update",
				"description" => "Update email templates data",
				"id_scope" => 3
			);
			$permission->insert($permission_row_3)->saveData();
		}
}
