<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 * description: This system.php creates all system paths
	 */
	/* HTTP Error Pages */
	$app->get('/403', function ($request, $response, $args) use ($app){
		$csrf_name_key = $this->csrf->getTokenNameKey();
		$csrf_value_key = $this->csrf->getTokenValueKey();
		$csrf_name = $request->getAttribute('csrf_name');
		$csrf_value = $request->getAttribute('csrf_value');
		require $_ENV["PBA_ADMIN_PAGES"].'/static/403.php';
		$response = $response->withStatus(403)->withAddedHeader('X-Status-Reason', 'Forbidden.');
		return $response;
	});

	$app->get('/404', function ($request, $response, $args) use ($app){
		$csrf_name_key = $this->csrf->getTokenNameKey();
		$csrf_value_key = $this->csrf->getTokenValueKey();
		$csrf_name = $request->getAttribute('csrf_name');
		$csrf_value = $request->getAttribute('csrf_value');
		require $_ENV["PBA_ADMIN_PAGES"].'/static/404.php';
		$response = $response->withStatus(404)->withAddedHeader('X-Status-Reason', 'Page not found.');
		return $response;
	});

	$app->get('/500', function ($request, $response, $args) use ($app){
		$csrf_name_key = $this->csrf->getTokenNameKey();
		$csrf_value_key = $this->csrf->getTokenValueKey();
		$csrf_name = $request->getAttribute('csrf_name');
		$csrf_value = $request->getAttribute('csrf_value');
		require $_ENV["PBA_ADMIN_PAGES"].'/static/500.php';
		$response = $response->withStatus(500)->withAddedHeader('X-Status-Reason', 'Internal server error.');
		return $response;
	});
?>
