<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class UserNotificationData extends Data {
		const table = "app.user_notification_data";

		private $id;
		private $id_user_notification;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".UserNotificationData::table." WHERE id = :id";
				$user_notification_data = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($user_notification_data["id"]);
				$this->setIdUserNotification($user_notification_data["id_user_notification"]);
				$this->setReferenceClass($user_notification_data["id_reference_class"]);
				$this->setObjectId($user_notification_data["object_id"]);
				$this->setCreatedAt($user_notification_data["created_at"]);
			}
		}

		public static function create($obj){
			if (UserNotificationData::validate($obj)){
				$user_notification_data = new UserNotificationData();

				$user_notification_data->setIdUserNotification(intval($obj["id_user_notification"]));

				$user_notification_data->setReferenceClass(intval($obj["id_reference_class"]));

				$user_notification_data->setObjectId(intval($obj["object_id"]));

				$data = array(
					"id_user_notification" => $user_notification_data->getIdUserNotification(),
					"id_reference_class" => $user_notification_data->getReferenceClass()->getId(),
					"object_id" => $user_notification_data->getObjectId()
				);
				$sql = "INSERT INTO ".UserNotificationData::table."(id_user_notification, id_reference_class, object_id) VALUES (:id_user_notification, :id_reference_class, :object_id) RETURNING id";

				$result = Database::execute($sql, $data);
				if ($result){
					$id = $result->fetch()["id"];
					$user_notification_data->setId($id);
					return $user_notification_data;
				}
			}
			return false;
		}

		public static function validate($obj){
			if (isset($obj["id_user_notification"]) &&
					isset($obj["id_reference_class"]) &&
					isset($obj["object_id"])){
				$user_notification = new UserNotification(intval($obj["id_user_notification"]));
				if (!$user_notification->getId()) return false;
				$reference_class = new ReferenceClass(intval($obj["id_reference_class"]));
				$object = $reference_class->instantiate($obj["object_id"]);
				if ($reference_class->getId() && $object && $object->getId()) return true;
			}
			return false;
		}

		public static function get($user_notification, $reference_class){
			if ($user_notification != null &&
					$user_notification->getId() &&
					$reference_class != null &&
					$reference_class->getId()){
				$sql = "SELECT id FROM ".UserNotificationData::table." WHERE id_user_notification = :id_user_notification AND id_reference_class = :id_reference_class";
				$result = Database::execute($sql, array("id_user_notification" => $user_notification->getId(), "id_reference_class" => $reference_class->getId()));
				if ($result){
					$id = $result->fetch()["id"];
					$user_notification_data = new UserNotificationData($id);
					if ($user_notification_data->getId()) return $user_notification_data;
				}
			}
			return false;
		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function setIdUserNotification($id_user_notification){
			$this->id_user_notification = $id_user_notification;
		}
		public function getIdUserNotification(){
			return $this->id_user_notification;
		}

		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
		public function getCreatedAt(){
			return $this->created_at;
		}
	}
?>
