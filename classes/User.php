<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class User {
		const table = "app.user";

		const system = 1;

		private $id;
		private $name;
		private $email;
		private $username;
		private $password;
		private $profile_picture;
		private $bio;
		private $linkedin;
		private $facebook;
		private $twitter;
		private $youtube;
		private $email_confirmed;
		private $reset_password;
		private $email_confirmation;
		private $notify_system_events;
		private $role;
		private $created_at;

		/*
		 *
		 * User Object Methods
		 *
		 */
		public function __construct($id = null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".User::table." WHERE id = :id";
				$user = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($user["id"]);
				$this->setName($user["name"]);
				$this->setEmail($user["email"]);
				$this->setUsername($user["username"]);
				$this->setPassword($user["password"]);
				$this->setProfilePicture($user["id_profile_picture"]);
				$this->setBio($user["bio"]);
				$this->setLinkedin($user["linkedin"]);
				$this->setFacebook($user["facebook"]);
				$this->setTwitter($user["twitter"]);
				$this->setYoutube($user["youtube"]);
				$this->setResetPassword($user["reset_password"]);
				$this->setEmailConfirmation($user["email_confirmation"]);
				$this->setNotifySystemEvents($user["notify_system_events"]);
				$email_confirmed = false;
				if ($user["email_confirmation"] == null) $email_confirmed = true;
				$this->setEmailConfirmed($email_confirmed);
				$this->setRole($user["id_role"]);
				$this->setCreatedAt($user["created_at"]);
			}
		}

		public function getId(){
			return $this->id;
		}

		private function setId($id){
			$this->id = $id;
		}

		public function getName(){
			return $this->name;
		}
		public function setName($name){
			$this->name = $name;
		}

		public function getEmail(){
			return $this->email;
		}
		public function setEmail($email){
			$this->email = $email;
		}

		public function getUsername(){
			return $this->username;
		}
		public function setUsername($username){
			$this->username = $username;
		}

		private function getPassword(){
			return $this->password;
		}
		public function setPassword($password){
			$this->password = $password;
		}

		public function getProfilePicture(){
			return $this->profile_picture;
		}
		public function setProfilePicture($profile_picture){
			$this->profile_picture = new Image($profile_picture);
		}

		public function showProfilePicture(){
			if ($this->getProfilePicture()->getId()){
				return $this->getProfilePicture()->getHighResolution();
			}
			return User::defaultProfilePicture();
		}

		public static function defaultProfilePicture(){
			return $_ENV["hostname"].$_ENV["PBA_ADMIN_IMAGE_FOLDER"].'user_avatar.png';
		}

		public function getBio(){
			return $this->bio;
		}
		public function setBio($bio){
			$this->bio = $bio;
		}

		public function getLinkedin(){
			return $this->linkedin;
		}
		public function setLinkedin($linkedin){
			$this->linkedin = $linkedin;
		}

		public function getFacebook(){
			return $this->facebook;
		}
		public function setFacebook($facebook){
			$this->facebook = $facebook;
		}

		public function getTwitter(){
			return $this->twitter;
		}
		public function setTwitter($twitter){
			$this->twitter = $twitter;
		}

		public function getYoutube(){
			return $this->youtube;
		}
		public function setYoutube($youtube){
			$this->youtube = $youtube;
		}

		public function getEmailConfirmed(){
			return $this->email_confirmed;
		}
		public function setEmailConfirmed($email_confirmed){
			$this->email_confirmed = $email_confirmed;
		}

		private function getEmailConfirmation(){
			return $this->email_confirmation;
		}
		private function setEmailConfirmation($email_confirmation){
			$this->email_confirmation = $email_confirmation;
		}

		public function getEmailConfirmationLink(){
			return $_ENV["hostname"].'/confirm/'.$this->getEmailConfirmation();
		}

		private function getResetPassword(){
			return $this->reset_password;
		}
		private function setResetPassword($reset_password){
			$this->reset_password = $reset_password;
		}

		public function getResetPasswordLink(){
			return $_ENV["hostname"].'/reset/'.$this->getResetPassword();
		}

		public function getNotifySystemEvents(){
			return (bool)$this->notify_system_events;
		}
		public function setNotifySystemEvents($notify_system_events){
			$this->notify_system_events = (bool)$notify_system_events;
		}

		public function getRole(){
			return $this->role;
		}
		public function setRole($role){
			$this->role = new Role($role);
		}

		public function getCreatedAt(){
			return $this->created_at;
		}
		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}

		public function getUnreadNotifications($type){
			if ($this->getId()){
				$sql = "SELECT UN.id FROM (SELECT * FROM ".UserNotification::table." WHERE id_user = :id_user AND read = 'f') UN INNER JOIN (SELECT * FROM ".Notification::table." WHERE type=:type) N ON UN.id_notification = N.id ORDER BY UN.created_at DESC";
				$unread_notifications = Database::execute($sql, array("id_user" => $this->getId(), "type" => $type));
				if ($unread_notifications){
					$i = 0;
					$unread_notifications_list = array();
					foreach ($unread_notifications as $unread_notification){
						$unread_notifications_list[$i] = new UserNotification($unread_notification["id"]);
						$i++;
					}
					return $unread_notifications_list;
				}
			}
			return false;
		}

		/*
		 * jQuery DataTables Plugin
		 */
		public static function dataTable($request){
			$columns = array(
				0 => "name",
				1 => "username",
				2 => "created_at",
				3 => "action"
			);

			$order_by = "";
			if (count($request["order"])>0){
				$i = 0;
				$order_by = " ORDER BY ";
				foreach ($request["order"] as $order){
					if ($i>0) $order_by.=",";
					$order_by.= $columns[$order["column"]]." ".$order["dir"];
					$i++;
				}
			}

			if ($request["search"]["value"]!='') {
				$sql = "SELECT id, name, username, to_char(created_at,'DD/MM/YYYY HH24:MI:SS') as created_at FROM ".User::table." WHERE (name LIKE :search OR username LIKE :search OR to_char(created_at,'DD/MM/YYYY HH24:MI:SS') LIKE :search) $order_by LIMIT :limit OFFSET :offset";
				$data = array("search" => '%'.$request["search"]["value"].'%', "limit" => $request["length"], "offset" => $request["start"]);

				$sql_filtered = "SELECT count(id) FROM ".User::table." WHERE (name LIKE :search OR username LIKE :search OR to_char(created_at,'DD/MM/YYYY HH24:MI:SS')) LIKE :search";
				$data_filtered = array("search" => '%'.$request["search"]["value"].'%');
			}else{
				$sql = "SELECT id, name, username, to_char(created_at,'DD/MM/YYYY HH24:MI:SS') as created_at FROM ".User::table." $order_by LIMIT :limit OFFSET :offset";
				$data = array("limit" => $request["length"], "offset" => $request["start"]);

				$sql_filtered = "SELECT count(id) FROM ".User::table;
				$data_filtered = null;
			}


			$users = Database::execute($sql, $data)->fetchAll();
			$filtered = Database::execute($sql_filtered, $data_filtered)->fetch()["count"];

			$i = 0;
			$users_list = array();
			foreach ($users as $user){
				$users_list[$i][0] = $user["name"];
				$users_list[$i][1] = $user["username"];
				$users_list[$i][2] = $user["created_at"];
				$users_list[$i][3] = '<div class=""><div class="btn-group"><button name="user-view" id="user_view_'.$user["id"].'" value="/users/view/'.$user["id"].'" class="link table-action btn btn-default"><i class="fa fa-search"></i></button><button name="user-edit" id="user_edit_'.$user["id"].'" value="/users/edit/'.$user["id"].'" class="link table-action btn btn-default"><i class="fas fa-pencil-alt"></i></button></div></div>';
				$i++;
			}

			$total = Database::execute("SELECT count(id) FROM ".User::table)->fetch()["count"];

			$response = array(
				"draw" => intval($request["draw"]),
				"recordsTotal" => $total,
				"recordsFiltered" => $filtered,
				"data" => $users_list
			);
			return $response;
		}

		/*
		 * User Create
		 */
		public static function create($obj){
			/* Check if object is valid */
			if (isset($obj['name']) &&
				isset($obj['email']) &&
				isset($obj['username']) &&
				isset($obj['password']) &&
				isset($obj['role']) &&
				User::validate($obj['name'],$obj['email'],$obj['username'],$obj['password'],$obj["role"])){

				/* Creating new user */
				$user = new User();
				$user->setName(htmlspecialchars($obj['name']));
				$user->setEmail(strtolower($obj['email']));
				$user->setUsername($obj['username']);
				$user->setPassword(password_hash($obj['password'], PASSWORD_BCRYPT));
				/* Generates random string for the email confirmation token */
				$email_confirmation = Crypt::random_string(32);
				$user->setEmailConfirmation($email_confirmation);

				$profile_picture = new Image(intval($obj["profile_picture"]));
				$user->setProfilePicture($profile_picture->getId());
				$user->setBio($obj["bio"]);
				$user->setLinkedin($obj["linkedin"]);
				$user->setFacebook($obj["facebook"]);
				$user->setTwitter($obj["twitter"]);
				$user->setYoutube($obj["youtube"]);

				$user->setNotifySystemEvents(false);
				if (isset($obj["notify_system_events"])) $user->setNotifySystemEvents($obj["notify_system_events"]);

				$role = new Role(intval($obj["role"]));
				$user->setRole($role->getId());

				/* Insert new user into database */
				$data = array(
						"name" => $user->getName(),
						"email" => $user->getEmail(),
						"username" => $user->getUsername(),
						"password" => $user->getPassword(),
						"id_profile_picture" => $user->getProfilePicture()->getId(),
						"bio" => $user->getBio(),
						"linkedin" => $user->getLinkedin(),
						"facebook" => $user->getFacebook(),
						"twitter" => $user->getTwitter(),
						"youtube" => $user->getYoutube(),
						"email_confirmation" => $email_confirmation,
						"notify_system_events" => (int)$user->getNotifySystemEvents(),
						"id_role" => $user->getRole()->getId()
				);
				$sql = "INSERT INTO ".User::table."(name, email, username, password, id_profile_picture, bio, linkedin, facebook, twitter, youtube, email_confirmation, notify_system_events, id_role) VALUES (:name, :email, :username, :password, :id_profile_picture, :bio, :linkedin, :facebook, :twitter, :youtube, :email_confirmation, :notify_system_events, :id_role) RETURNING id";
				$id =  Database::execute($sql, $data)->fetch()["id"];

 				/* If user was successfully created, send e-mail confirmation */
				if ($id){
					$user->setId($id);
					if ($user->sendEmailConfirmation()) return $user;
				}
			}
			return false;
		}

		private function sendEmailConfirmation(){
			if (EmailUser::addToQueue(EmailUser::email_confirmation, $this, array($this))) return true;
			Log::error("PBA ERROR: could not create email confirmation");
			return false;
		}


		public static function find(){
			$sql = "SELECT id FROM ".User::table." ORDER BY name";
			$users = Database::execute($sql)->fetchAll();

			$i = 0;
			$users_list = array();
			foreach ($users as $user) {
				$users_list[$i] = new User($user["id"]);
				$i++;
			}
			return $users_list;
		}

		public function update(){
			if (User::validate($this->getName(), $this->getEmail(), $this->getUsername(), $this->getPassword(), $this->getRole(), $this)){
					$data = array(
						"name" => $this->getName(),
						"email" => $this->getEmail(),
						"username" => $this->getUsername(),
						"password" => $this->getPassword(),
						"id_profile_picture" => $this->getProfilePicture()->getId(),
						"bio" => $this->getBio(),
						"linkedin" => $this->getLinkedin(),
						"facebook" => $this->getFacebook(),
						"twitter" => $this->getTwitter(),
						"youtube" => $this->getYoutube(),
						"notify_system_events" => (int)$this->getNotifySystemEvents(),
						"id_role" => $this->getRole()->getId(),
						"id" => $this->getId()
					);
					$sql = "UPDATE ".User::table." SET name = :name, email = :email, username = :username, password = :password, id_profile_picture = :id_profile_picture, bio = :bio, linkedin = :linkedin, facebook = :facebook, twitter = :twitter, youtube = :youtube, notify_system_events = :notify_system_events, id_role = :id_role WHERE id = :id";
					return Database::execute($sql, $data);
			}
			return false;
		}


		public function toArray(){
			$user = array(
				"id" => $this->getId(),
				"name" => $this->getName(),
				"email" => $this->getEmail(),
				"username" => $this->getUsername(),
				"bio" => $this->getBio(),
				"linkedin" => $this->getLinkedin(),
				"facebook" => $this->getFacebook(),
				"twitter" => $this->getTwitter(),
				"youtube" => $this->getYoutube(),
				"email_confirmed" => $this->getEmailConfirmed()
			);
			if ($this->getProfilePicture() && $this->getProfilePicture()->getId()){
				$user["profile_picture"] = $this->getProfilePicture()->getHighResolution();
			}
			return $user;
		}
		/*
		 *
		 * User Static Methods
		 *
		 */
		public function getSubscribedToNotification($notification){
			if ($notification->getId()){
				$sql = "";
				switch($notification->getType()){
					case Notification::system:
						$sql = "SELECT id FROM ".User::table." WHERE notify_system_events = 't'";
					break;
				}
				if ($sql){
					$users = Database::execute($sql)->fetchAll();
					if ($users && count($users)>0){
						$i = 0;
						$users_list = array();
						foreach ($users as $user){
							$users_list[$i] = new User(intval($user["id"]));
							$i++;
						}
						return $users_list;
					}
				}
			}
			return false;
		}

		private static function validate($name,$email,$username,$password,$role,$user=null){
			/* Role validation */
			if (!is_a($role,"Role")){
				$role = new Role(intval($role));
			}
			if (!$role->getId()) return false;
			/*
			 * Validate html and php tags
			 *
			 * */
			if ($name == strip_tags($name) || $email == strip_tags($email) || $username == strip_tags($username)){
				/*
				 * Validate name
				 *
				 * */
				$validate_name_options = array(
					'options' => array(
							'min_range' => 3,
							'max_range' => 500,
					)
				);
				$validate_name_size = filter_var(strlen($name), FILTER_VALIDATE_INT, $validate_name_options);
				if ($validate_name_size === false) return false;
				/*
				 * Validate e-mail
				 *
				 * */
				if (User::validateEmail($email, $user) === false) return false;
				/*
				 * Validate username
				 *
				 * */
				if (User::validateUsername($username, $user) === false) return false;
				/*
				 * Validate password
				 *
				 * */
				/* Every password must have at least 6 characters... */
				if (empty($password) || (strlen($password)<6 && strlen($password)>255)) return false;

				/* ...at least one uppercase...
				if (preg_match_all('/[A-Z]/',$password)<1) return false;

				/* ...at least one lowercase...
				if (preg_match_all('/[a-z]/',$password)<1) return false;

				/* ...at least one special character...
				if (preg_match_all('/[!@#$%^&*()\-_=+{};:,<.>]/',$password)<1) return false;

				/* ...at least one number...
				if (preg_match_all('/[0-9]/',$password)<1) return false;*/

				return true;
			}
			return false;
		}

		public static function validateUsername($username, $user=null){
			if ($username!=null){
				$validate_username_options = array(
						'options' => array(
								'min_range' => 3,
								'max_range' => 33,
								'regexp' => '/^[a-zA-Z0-9_.-]*$/'
						)
				);
				$validate_username_size = filter_var(strlen($username),FILTER_VALIDATE_INT,$validate_username_options);
				$validate_username_letters = filter_var($username,FILTER_VALIDATE_REGEXP,$validate_username_options);
				if ($validate_username_size == false || $validate_username_letters === false) return false;

				/* Check if username is already taken */
				$sql = "SELECT count(id) FROM ".User::table." WHERE username = :username";
				$users = Database::execute($sql, array('username' => $username))->fetch()["count"];
				if ($users == 0) return true;

				if ($users == 1){
					/* User own username */
					$sql = "SELECT id FROM ".User::table." WHERE username = :username";
					$user_id = Database::execute($sql, array('username' => $username))->fetch()["id"];
					if ($user != null && $user->getId() == $user_id) return true;
				}
			}

			return false;
		}

		public static function validateEmail($email, $user=null){
			if ($email!=null){

				$validate_email = filter_var($email, FILTER_VALIDATE_EMAIL);
				if ($validate_email === false) return false;

				/* Check if e-mail is already taken */
				$sql = "SELECT count(id) FROM ".User::table." WHERE email = :email";
				$emails = Database::execute($sql, array("email" => $email))->fetch()["count"];
				if ($user == null && $emails === 0) return true;

				/* Covers when user already exists or is changing his/her email */
				if ($user != null && $emails <= 1) return true;
			}

			return false;
		}

		public static function authenticate($username, $password){
			$sql = "SELECT id, password FROM ".User::table." WHERE username = :username";
			$user = Database::execute($sql, array("username" => $username))->fetch();
			if ($user && count($user) != 0){
				if (password_verify($password, $user["password"])){
					$_SESSION["user"] = new User($user["id"]);
					return true;
				}
			}
			return false;
		}

		public static function confirmEmail($token){
			$sql = "UPDATE ".User::table." SET email_confirmation = :email_confirmation WHERE email_confirmation = :token";
			return Database::execute($sql, array("email_confirmation" => null, "token" => $token));
		}

		public static function validateResetToken($token){
			$sql = "SELECT id FROM ".User::table." WHERE reset_password = :reset_password ";
			$user = Database::execute($sql, array('reset_password' => $token))->fetch();
			if (isset($user["id"]) && $user["id"]){
				return true;
			}
			return false;
		}

		public static function resetPassword($token, $password){
			$sql = "UPDATE ".User::table." SET reset_password = :reset_password, password = :password WHERE reset_password = :token";
			$data = array("reset_password" => null, "password" => password_hash($password, PASSWORD_BCRYPT), "token" => $token);
			return Database::execute($sql, $data);
		}

		public static function askResetPassword($email){
			$reset_password = Crypt::random_string(32);
			$sql = "UPDATE ".User::table." SET reset_password = :reset_password WHERE email = :email";
			$data = array("reset_password" => $reset_password, "email" => $email);
			if (Database::execute($sql, $data)){
				$sql = "SELECT id FROM ".User::table." WHERE email = :email";
				$id = Database::execute($sql, array("email" => $email))->fetch()["id"];
				$user = new User($id);
				return $user->sendResetPassword();
			}
			return false;
		}

		private function sendResetPassword(){
			if (EmailUser::addToQueue(EmailUser::reset_password, $this, array($this))) return true;
			Log::error("PBA ERROR: could not create reset password email");
			return false;
		}

		/* Checks if a user is allowed to do something */
		public function isAllowed($permission){
			if ($permission && $permission > 0 && $this->getId() && $this->getRole() && $this->getRole()->getId()){
				return $this->getRole()->isPermitted($permission);
			}
			return false;
		}
	}
?>
