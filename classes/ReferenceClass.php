<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
  class ReferenceClass{
    const table = "app.reference_class";

    const User = 1;
    const Page = 2;
    const Click = 3;
    const Hook = 4;
    const Gift = 5;
    const Lead = 6;

    private $id;
    private $title;
    private $description;
    private $name;
    private $created_at;

    public function __construct($id=null){
      if (is_int($id)){
        $sql = "SELECT * FROM ".ReferenceClass::table." WHERE id = :id";
        $reference_class = Database::execute($sql, array("id" => $id))->fetch();
      }else{
        $sql = "SELECT * FROM ".ReferenceClass::table." WHERE name = :id";
        $reference_class = Database::execute($sql, array("id" => $id))->fetch();
      }
      $this->setId($reference_class["id"]);
      $this->setTitle($reference_class["title"]);
      $this->setDescription($reference_class["description"]);
      $this->setName($reference_class["name"]);
      $this->setCreatedAt($reference_class["created_at"]);
    }

    public function call($method, $params){
      if ($method != null && $params !== null){
        $class = StringUtils::toCamelCase($this->getName(), true);
        $method = new ReferenceClassMethod($this->getId(), $method);
        $get_method = "get".StringUtils::toCamelCase($method->getName(), true);
        $obj = $class::$get_method($params);
        return $obj;
      }
      return false;
    }

    public function instantiate($object_id){
      if ($object_id && $this->getId()){
        $class = StringUtils::toCamelCase($this->getName(), true);
        return new $class(intval($object_id));
      }
      return false;
    }

    public function render($obj, $field){
      if ($obj && $obj->getId() && strlen($field)>0){
        $reference_class_field = new ReferenceClassField($this->getId(), $field);
        if ($reference_class_field->getId()){
          $get_value = "get".StringUtils::toCamelCase($reference_class_field->getName(), true);
          return $obj->$get_value();
        }
      }
      return false;
    }

    public static function get($obj){
      $reference_class = new ReferenceClass(StringUtils::fromCamelToUnderline(get_class($obj)));
      if ($reference_class && $reference_class->getId()) return $reference_class;
      return false;
    }

    private function setId($id){
      $this->id = $id;
    }
    public function getId(){
      return $this->id;
    }

    public function getTitle(){
      return $this->title;
    }
    private function setTitle($title){
      $this->title = $title;
    }

    public function getDescription(){
      return $this->description;
    }
    private function setDescription($description){
      $this->description = $description;
    }

    public function getName(){
      return $this->name;
    }
    private function setName($name){
      $this->name = $name;
    }

    private function getCreatedAt(){
      return $this->created_at;
    }
    public function setCreatedAt($created_at){
      $this->created_at = new Datetime($created_at);
    }
  }
?>
