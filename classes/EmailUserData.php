<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class EmailUserData extends Data {
		const table = "app.email_user_data";

		private $id;
		private $id_email_user;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".EmailUserData::table." WHERE id = :id";
				$email_user_data = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($email_user_data["id"]);
				$this->setIdEmailUser($email_user_data["id_email_user"]);
				$this->setReferenceClass($email_user_data["id_reference_class"]);
				$this->setObjectId($email_user_data["object_id"]);
				$this->setCreatedAt($email_user_data["created_at"]);
			}
		}

		public static function create($obj){
			if (EmailUserData::validate($obj)){
				$email_user_data = new EmailUserData();

				$email_user_data->setIdEmailUser(intval($obj["id_email_user"]));

				$email_user_data->setReferenceClass(intval($obj["id_reference_class"]));

				$email_user_data->setObjectId(intval($obj["object_id"]));

				$data = array(
					"id_email_user" => $email_user_data->getIdEmailUser(),
					"id_reference_class" => $email_user_data->getReferenceClass()->getId(),
					"object_id" => $email_user_data->getObjectId()
				);
				$sql = "INSERT INTO ".EmailUserData::table."(id_email_user, id_reference_class, object_id) VALUES (:id_email_user, :id_reference_class, :object_id) RETURNING id";

				$result = Database::execute($sql, $data);
				if ($result){
					$id = $result->fetch()["id"];
					$email_user_data->setId($id);
					return $email_user_data;
				}
			}
			return false;
		}

		public static function validate($obj){
			if (isset($obj["id_email_user"]) &&
					isset($obj["id_reference_class"]) &&
					isset($obj["object_id"])){
				$email_user = new EmailUser(intval($obj["id_email_user"]));
				if (!$email_user->getId()) return false;
				$reference_class = new ReferenceClass(intval($obj["id_reference_class"]));
				$object = $reference_class->instantiate($obj["object_id"]);
				if ($reference_class->getId() && $object && $object->getId()) return true;
			}
			return false;
		}

		public static function get($email_user, $reference_class){
			if ($email_user != null &&
					$email_user->getId() &&
					$reference_class != null &&
					$reference_class->getId()){
				$sql = "SELECT id FROM ".EmailUserData::table." WHERE id_email_user = :id_email_user AND id_reference_class = :id_reference_class";
				$result = Database::execute($sql, array("id_email_user" => $email_user->getId(), "id_reference_class" => $reference_class->getId()));
				if ($result){
					$id = $result->fetch()["id"];
					$email_user_data = new EmailUserData($id);
					if ($email_user_data->getId()) return $email_user_data;
				}
			}
			return false;
		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function setIdEmailUser($id_email_user){
			$this->id_email_user = $id_email_user;
		}
		public function getIdEmailUser(){
			return $this->id_email_user;
		}

		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
		public function getCreatedAt(){
			return $this->created_at;
		}
	}
?>
