<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class Scope{
		const table = "app.scope";
		private $id;
		private $name;
		private $description;

		public function __construct($id=null){
			if ($id != null && $id > 0){
				$sql = "SELECT * FROM ".Scope::table." WHERE id = :id";
				$scope = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($scope["id"]);
				$this->setName($scope["name"]);
				$this->setDescription($scope["description"]);
			}
		}

		public function getId(){
			return $this->id;
		}
		public function getName(){
			return $this->name();
		}
		public function getDescription(){
			return $this->description();
		}
	}
?>
