<?php
	/*
	 * PBA - Copyright (c) 2011-2019 Jonathan Nunes Boilesen
	 *
	 *
	 * This software is Open Software.
	 *	This software is licensed under Apache License 2.0.
	 *
	 *
	 * author: Jonathan Nunes Boilesen
	 * date: 08/01/2016
	 */
	class EmailTemplate{
		const table = "app.email_template";

		private $id;
		private $title;
		private $description;
		private $subject;
		private $markdown;
		private $html_body;
		private $text_body;
		private $created_at;

		public function __construct($id=null){
			if (is_int($id) && $id>0){
				$sql = "SELECT * FROM ".EmailTemplate::table." WHERE id = :id";
				$email = Database::execute($sql, array("id" => $id))->fetch();
				$this->setId($email["id"]);
				$this->setTitle($email["title"]);
				$this->setDescription($email["description"]);
				$this->setSubject($email["subject"]);
				$this->setMarkdown($email["markdown"]);
				$this->setHtmlBody($email["html_body"]);
				$this->setTextBody($email["text_body"]);
				$this->setCreatedAt($email["created_at"]);
			}
		}

		/*
		 * jQuery DataTables Plugin
		 */
		public static function dataTable($request){
			$columns = array(
				0 => "title",
				1 => "description",
				2 => "created_at",
				3 => "action"
			);

			$order_by = "";
			if (count($request["order"])>0){
				$i = 0;
				$order_by = " ORDER BY ";
				foreach ($request["order"] as $order){
					if ($i>0) $order_by.=",";
					$order_by.= $columns[$order["column"]]." ".$order["dir"];
					$i++;
				}
			}

			if ($request["search"]["value"]!='') {
				$sql = "SELECT id, title, description, to_char(created_at,'DD/MM/YYYY HH24:MI:SS') as created_at FROM ".EmailTemplate::table." WHERE (title like :search OR description like :search OR to_char(created_at,'DD/MM/YYYY HH24:MI:SS') like :search) $order_by LIMIT :limit OFFSET :offset";
				$data = array("search" => '%'.$request["search"]["value"].'%', "limit" => $request["length"], "offset" => $request["start"]);

				$sql_filtered = "SELECT count(id) FROM ".EmailTemplate::table." WHERE (title like :search OR description like :search OR to_char(created_at,'DD/MM/YYYY HH24:MI:SS')) like :search";
				$data_filtered = array("search" => '%'.$request["search"]["value"].'%');
			}else{
				$sql = "SELECT id, title, description, to_char(created_at,'DD/MM/YYYY HH24:MI:SS') as created_at FROM ".EmailTemplate::table." $order_by LIMIT :limit OFFSET :offset";
				$data = array("limit" => $request["length"], "offset" => $request["start"]);

				$sql_filtered = "SELECT count(id) FROM ".EmailTemplate::table;
				$data_filtered = array();
			}

			$email_templates = Database::execute($sql, $data)->fetchAll();

			$filtered = Database::execute($sql_filtered, $data_filtered)->fetch()["count"];

			$i = 0;
			$email_templates_list = array();
			foreach ($email_templates as $email_template){
				$email_templates_list[$i][0] = $email_template["title"];
				$email_templates_list[$i][1] = $email_template["description"];
				$email_templates_list[$i][2] = $email_template["created_at"];
				$email_templates_list[$i][3] = '<div class=""><div class="btn-group"><button name="email-template-view" id="email_template_view_'.$email_template["id"].'" value="/emails/templates/view/'.$email_template["id"].'" class="link table-action btn btn-default"><i class="fa fa-search"></i></button><button name="email-template-edit" id="email_template_edit_'.$email_template["id"].'" value="/emails/templates/edit/'.$email_template["id"].'" class="link table-action btn btn-default"><i class="fas fa-pencil-alt"></i></button></div></div>';
				$i++;
			}

			$sql_total = "SELECT count(id) FROM ".EmailTemplate::table;
			$total = Database::execute($sql_total)->fetch()["count"];

			$response = array(
				"draw" => intval($request["draw"]),
				"recordsTotal" => $total,
				"recordsFiltered" => $filtered,
				"data" => $email_templates_list
			);
			return $response;
		}

		public static function find(){

			$sql = "SELECT id FROM ".EmailTemplate::table." ORDER BY title";
			$email_templates = Database::execute($sql)->fetchAll();
			$i = 0;
			$email_templates_list = array();

			try{
				foreach ($email_templates as $email_template){
					$email_templates_list[$i] = new EmailTemplate($email_template["id"]);
					$i++;
				}
			}catch(Exception $e){
				Log::error("PBA [500] FATAL: Could not find e-mail templates ".$e->getMessage());
			}
			return $email_templates_list;
		}

		/*
		 * E-mail Create
		 */
		public static function create($obj){
			/* Check if object is valid */
			if (EmailTemplate::validate($obj)){
				/* Creating new email */
				$email_template = new EmailTemplate();
				$email_template->setTitle($obj['title']);
				$email_template->setDescription($obj['description']);
				$email_template->setSubject($obj['subject']);
				$email_template->setMarkdown(false);
				if (isset($obj["markdown"])) $email_template->setMarkdown(true);
				$email_template->setHtmlBody($obj["html_body"]);
				$email_template->setTextBody($obj["text_body"]);

				/* Insert new email into database */
				$data = array(
						"title" => $email_template->getTitle(),
						"description" => $email_template->getDescription(),
						"subject" => $email_template->getSubject(),
						"markdown" => (int)$email_template->getMarkdown(),
						"html_body" => $email_template->getHtmlBody(),
						"text_body" => $email_template->getTextBody(),
				);
				$sql = "INSERT INTO ".EmailTemplate::table."(title, description, subject, markdown, html_body, text_body) VALUES (:title, :description, :subject, :markdown, :html_body, :text_body) RETURNING id";
				$id = Database::execute($sql, $data)->fetch()["id"];
				if (is_int($id) && $id > 0){
					$email_template->setId($id);
					return $email_template;
				}
			}
			return false;
		}

		/*
		 * Email Update
		 */
		public function update(){
			$data = array(
				"title" => $this->getTitle(),
				"description" => $this->getDescription(),
				"subject" => $this->getSubject(),
				"markdown" => (int)$this->getMarkdown(),
				"html_body" => $this->getHtmlBody(),
				"text_body" => $this->getTextBody(),
				"id" => $this->getId()
			);
			if (EmailTemplate::validate($data)){
				$sql = "UPDATE ".EmailTemplate::table." SET title = :title, description = :description, subject = :subject, markdown = :markdown, html_body = :html_body, text_body = :text_body WHERE id = :id";

				return Database::execute($sql, $data);
			}
			return false;
		}

		/*
		 * Email Template Validate
		 */
		public static function validate($obj){
			if (isset($obj["title"]) &&
					isset($obj["description"]) &&
					isset($obj["html_body"]) &&
					isset($obj["text_body"]) &&
					$obj["title"]!=null &&
					strlen($obj["title"])>2 &&
					$obj["description"]!=null &&
					strlen($obj["description"])>5 &&
					isset($obj["subject"]) &&
					strlen($obj["subject"])>1 &&
					strlen($obj["subject"])<256 &&
					strlen($obj["html_body"])>5 &&
					strlen($obj["html_body"])<100000 &&
					strlen($obj["text_body"])>5 &&
					strlen($obj["text_body"])<100000){
				return true;
			}
			return false;
		}

		private function setId($id){
			$this->id = $id;
		}
		public function getId(){
			return $this->id;
		}

		public function setTitle($title){
			$this->title = $title;
		}
		public function getTitle(){
			return $this->title;
		}

		public function setDescription($description){
			$this->description = $description;
		}
		public function getDescription(){
			return $this->description;
		}

		public function setSubject($subject){
			$this->subject = $subject;
		}
		public function getSubject(){
			return $this->subject;
		}

		public function setMarkdown($markdown){
			$this->markdown = (bool)$markdown;
		}
		public function getMarkdown(){
			return $this->markdown;
		}

		public function setHtmlBody($html_body){
			$this->html_body = $html_body;
		}
		public function getHtmlBody(){
			return $this->html_body;
		}

		public function setTextBody($text_body){
			$this->text_body = $text_body;
		}
		public function getTextBody(){
			return $this->text_body;
		}

		public function setCreatedAt($created_at){
			$this->created_at = new Datetime($created_at);
		}
		public function getCreatedAt(){
			return $this->created_at;
		}

		public function getHTML(){
			if ($this->getMarkdown()){
				$parse_down = new Parsedown();
		 		return $parse_down->text($this->getHtmlBody());
			}
			return $this->getHtmlBody();
		}

		public function toArray(){
			return array(
				"id" => $this->getId(),
				"title" => $this->getTitle(),
				"description" => $this->getDescription()
			);
		}
	}
?>
